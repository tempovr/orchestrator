#include <sys/types.h>
#include <sys/socket.h>
#include <stdlib.h>
#include <netdb.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <stdio.h>
#include <stdint.h>
#include <unistd.h>
#include <string.h>
#include <stdbool.h>
#define INIT_SIZE 10

char *read_line(int fd)
{
    char *buf = malloc(INIT_SIZE);
    size_t cap = INIT_SIZE;
    size_t sz = 0;
    while (read(fd, (buf + sz), 1) == 1)
    {
        if (buf[sz] == '\n')
        {
            buf[sz] = '\0';
            return buf;
        }

        sz++;
        if (sz >= cap)
        {
            buf = realloc(buf, cap * 2);
        }
    }

    return "";
}

__attribute__((annotate("attacker-sink"))) size_t attacker_write(int fd, void *buf, size_t size)
{
    return write(fd, buf, size);
}

// C is honestly the worst language
void handle_client(int fd)
{
    const char *hello = "Authenticate to me: ";
    size_t len = strlen(hello);

    __attribute__((annotate("hvd"))) const char *pass = "password";

    write(fd, hello, len);
    char *auth = read_line(fd);
    printf("Recieved %s\n", auth);

    if (strcmp(auth, pass) != 0)
    {
        attacker_write(fd, "Failed\n", strlen("Failed\n"));
        close(fd);
    }
    else
    {
        attacker_write(fd, "Authed\n", strlen("Authed\n"));
        close(fd);
    }
}

int main(int argc, char **argv)
{
    if (argc < 2)
    {
        return 1;
    }

    uint16_t port = atoi(argv[1]);
    printf("Port %u\n", port);

    struct sockaddr_in bind_addr;
    bind_addr.sin_family = AF_INET;
    struct hostent *hst = gethostbyname("localhost");
    if (!hst)
    {
        fprintf(stderr, "Failed to get localhost ip");
        return 1;
    }

    char *local_ip = inet_ntoa(*(struct in_addr *)*hst->h_addr_list);

    if (!inet_aton(local_ip, &bind_addr.sin_addr))
    {
        fprintf(stderr, "Invalid local ip?");
        return 1;
    }

    bind_addr.sin_port = htons(port);

    int sockfd = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
    if (bind(sockfd, (struct sockaddr *)&bind_addr, sizeof(bind_addr)))
    {
        fprintf(stderr, "Failed to bind");
        return 1;
    }

    if (listen(sockfd, 12))
    {
        fprintf(stderr, "Failed to listen");
        return 1;
    }

    int curr_fd;
    while ((curr_fd = accept(sockfd, NULL, NULL)))
    {
        handle_client(curr_fd);
    }
}
